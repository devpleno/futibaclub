const express = require('express')
const app = express()
const session = require('express-session')
const bodyParser = require('body-parser')
const mysql = require('mysql2/promise')

app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'))
app.set('view engine', 'ejs')

const account = require('./account')
const admin = require('./admin')
const groups = require('./groups')

app.use(session({
    secret: 'fsa',
    saveUninitialized: true,
    resave: true
}))

const confsDatabase = {
    port: '3306',
    host: '127.0.0.1',
    user: 'root',
    password: 'root',
    database: 'fsa3'
}

const init = async () => {
    const connection = await mysql.createConnection(confsDatabase);

    app.use((req, res, next) => {
        (req.session.user) ?
            res.locals.user = req.session.user :
            res.locals.user = false
        next()
    })

    app.use(account(connection))
    app.use('/admin', admin(connection))
    app.use('/groups', groups(connection))

    app.get('/classification', async (req, res) => {
        const query = `
            select
            users.id, 
            users.name,
            sum(guessings.score) as score
        from users
        left join
            guessings
                on guessings.user_id = users.id
        group by users.id
        order by score DESC
        `

        const [rows] = await connection.execute(query)
        res.render('classification', { ranking: rows })
    })

    app.listen(3000, err => {
        if (err) {
            console.log('Não foi possivel iniciar')
        } else {
            console.log('Express rodando na porta 3000...')
        }
    })
}

init();